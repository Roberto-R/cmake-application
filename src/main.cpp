#include <iostream>
#include <MyClass.h>

int main() {

    std::cout << "Hello world!" << std::endl;

    MyClass::whoAmI();

    MyClass object(7);

    std::cout << "Value is: " << object.getValue() << std::endl;

    return 0;
}