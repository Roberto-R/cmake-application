# CMake Application #

Example of a CMake program that includes a CMake library as a git submodule.  
The library is <https://bitbucket.org/Roberto-R/cmake-library>

The subdirectory is an alternative to a located library. They work independently, hence the need for `target_link_library`. But because of the CMake structure the library headers can be included directly.

Note that the library could also have install targets or unit tests. In that case the library could also be installed first, instead of included as a submodule. Look for other examples on how to do this, this example is kept as simple as possible.

## Install ##

Get started with:

1. Clone repository including submodules: `git clone url --recursive`
   * Or checkout submodules if you forgot the recursive flags: `git submodule update --init`
2. `mkdir build && cd build`
3. `cmake ..`
4. `make`
